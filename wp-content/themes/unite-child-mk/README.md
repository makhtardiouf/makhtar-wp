# Wordpress theme customization task for GiteSoft #

Theme Name: Unite-Child-Makhtar

Theme URI: https://bitbucket.org/makhtardiouf/makhtar-wp/

Version: 0.1

Tested up to: WP 4.9 and MySQL 5.7

#Description#

This is a Custom child theme derived from Unite.
It allows to add Film pages, items, and setting taxonimies for films: Genre, Country, Year and Actors. As well as the following custom fields: "Country", "Genre", "Ticket Price", "Release Date".

The files of the child theme are in makhtar-wp/wp-content/themes/unite-child-mk/

The file makhtar-wp/wp-content/themes/unite-child-mk/functions.php defines hooks for registering the elements decribed above. 

It also defines the shortcode 'last5films' for listing the last 5 films recorded (listed in the index page).

The files content-single, archive (list of films) and single-film.php were created to custom the presentation of the contents as per the Assignements.

The plugin "Advanced Custom Fields" was also installed to facilitate the manipulation of custom fields.


#Installation#

Retrieve the complete wordpress archive with:

git clone https://bitbucket.org/makhtardiouf/makhtar-wp

Import the database dump makhtar-wp/DB/makhtar-wp20171125.sql

It already includes the "CREATE DATABASE makhtar-wp" statement.

Add permissions for user "demo" by running the following SQL statement:

GRANT ALL PRIVILEGES on `makhtar-wp`.* TO demo@'localhost' IDENTIFIED BY ';demo;' ;

# Testing
You can use the php development server to access the UI, e.g.:

cd makhtar-wp; php -S localhost:8200

Then open a browser tab with the address http://localhost:8200

*** Login with user "admin" and password ";demo;"
