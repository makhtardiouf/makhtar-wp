<?php
/**
 * Makhtar Diouf
 * Unite Child theme for GiteSoft
 */
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function enqueue_parent_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css');
}

// Register the Custom Film Post Type
 
function register_mk_film()
{
    $labels = array(
        'name' => _x('Films', 'film'),
        'singular_name' => _x('Film', 'film'),
        'add_new' => _x('Add New', 'film'),
        'add_new_item' => _x('Add New Film', 'film'),
        'edit_item' => _x('Edit Film', 'film'),
        'new_item' => _x('New Film', 'film'),
        'view_item' => _x('View Film', 'film'),
        'search_items' => _x('Search Films', 'film'),
        'not_found' => _x('No Films found', 'film'),
        'not_found_in_trash' => _x('No Films found in Trash', 'film'),
        'parent_item_colon' => _x('Parent Film:', 'film'),
        'menu_name' => _x('Films', 'film'),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Films filterable by genre',
        'supports' => array( 'title', 'editor', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'page-attributes' ),
        'taxonomies' => array( 'genres', 'country', 'year', 'actors' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-video',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type('film', $args);
}
 
add_action('init', 'register_mk_film');


function film_taxonomies()
{
    register_taxonomy(
        'genres',
        'country', 
        'year', 
        'actors',
        'film',
        array(
            'hierarchical' => true,
            'label' => 'Taxonomies',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'Taxonomies',
                'with_front' => false
            )
        )
    );
}
add_action('init', 'film_taxonomies');


// Shortcode for last 5 films
function lastFilms() {
    $args = array(
        'sort_order' => 'desc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => -1,
        'exclude_tree' => '',
        'number' => 5,
        'offset' => 0,
        'post_type' => 'film',
        'post_status' => 'publish'
    ); 
    $pages = get_pages($args); 
    foreach( $pages as $page ) {		
		$content = $page->post_content;
		if ( ! $content ) 
			continue;

		$content = apply_filters( 'the_content', $content );
	?>

		<h6><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></h6>		
       
	<?php
	}	
}

add_shortcode('last5films', 'lastFilms');
