<?php
/**
 * The template for displaying List of Films
 *
 * Makhtar Diouf
 *
 * @package unite-child-mk
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
		<main id="main" class="site-main" role="main">

		<?php 
		$targets = array("Country", "Genre", "Ticket Price", "Release Date");

		if ( have_posts() ) : ?>

			<header class="page-header">
				<h2>List of registered Films</h2>
			</header><!-- .page-header -->

			<?php while ( have_posts() ) : the_post(); 

				/* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
				get_template_part( 'content', get_post_format() );

				echo "<div style='top:-100px; padding:20px;'>";
				the_meta();
				echo "</div>";
				?>

			<?php endwhile; ?>

			<?php unite_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
