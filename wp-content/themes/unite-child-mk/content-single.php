<?php
/**
 * Custom single page content
 * Makhtar Diouf
 *
 * @package unite-child-mk
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">

		<?php 
			if (of_get_option('single_post_image', 1) == 1) :
				the_post_thumbnail('unite-featured', array( 'class' => 'thumbnail' ));
			endif;
		?>

		<h1 class="entry-title "><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php unite_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
            wp_link_pages(array(
                'before' => '<div class="page-links">' . __('Pages:', 'unite'),
                'after'  => '</div>',
			));
			
			// Display custom fields under the description
			the_meta();
        ?>

	</div><!-- .entry-content -->

	<footer class="entry-meta">
		
		<?php edit_post_link(__('Edit', 'unite'), '<i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span>'); ?>
		<?php unite_setPostViews(get_the_ID()); ?>
		<hr class="section-divider">
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
