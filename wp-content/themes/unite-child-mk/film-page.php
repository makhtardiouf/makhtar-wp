<?php
/**
 * Template Name: Custom Film page
 *
 * Makhtar Diouf
 *
 * @package unite-child-mk
 */
get_header(); ?>

<div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option('site_layout'); ?>">
	<main id="main" class="site-main" role="main">

		<?php while (have_posts()) : the_post(); ?>

			<?php 
			get_template_part('content', 'page');
			
			// Better custom fields display than the_meta();
			$field_keys = array("country", "genre", "ticket_price", "release_date");
			foreach ($field_keys as $k) {
				$fields = get_field_object($k);

				if ($fields) {
					echo $fields['label'] . ": " . $fields['value'] . "<br>  ";
				}
			}			
?>
				
		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

