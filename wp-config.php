<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'makhtar-wp');

/** MySQL database username */
define('DB_USER', 'demo');

/** MySQL database password */
define('DB_PASSWORD', ';demo;');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q.sya 5-=1eynYyz 4!&mUWcUR+R{B&gZ7DR>;FrPylh@C}:OI$s5b6%XFq4I+zd');
define('SECURE_AUTH_KEY',  'x8]ZzfQ;j(46^CjRUA#s?TQ=BjzC[sp%FaqEaD]qD(H`B~yBr,Xcb8t(acEjt-MA');
define('LOGGED_IN_KEY',    '[9=qV(Bh<Bp1Ho9OvNu#QD&,$GPZX<+,A88&hYH<VSu4KuJuMq^z!/%SP2h;JdRY');
define('NONCE_KEY',        '}G8BNGAQm1 7@ghOjV3{w|K<]<^8tHG~!0_NPd=3GQz$+.V[rQ$,:uAn)ZB~ms#/');
define('AUTH_SALT',        'y>TmUJXN?g-Hfw`O2~#]X$O$m]ox@XHd/_*5E=|:wfTF`*nI&c/V%7hF}(<<1%xS');
define('SECURE_AUTH_SALT', '-2qanVhQ}4/dyE#.-H<Fs_Oh]W9X$Nf~i5=o!6A_whPbzp0P,(`G;U!i(B0JRAUZ');
define('LOGGED_IN_SALT',   'C>M6pN&UUIpnJ[x43Wv|;?nXlj0.@|?5 J NxoZ?T$NFd$dWF)cT,cWpBMp1!(k=');
define('NONCE_SALT',       'cB4BIl/YD8T?|;0?k%RCqhPZd?l*M9.St!~Mic$9BM^mUjU/U8~Hh|SvJ6Xy(54,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
